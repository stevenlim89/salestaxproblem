import java.util.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Item{
    private HashMap<String, Integer> exemptList;
    private String value;
    private BigDecimal taxRate;
    private BigDecimal importRate;
    private BigDecimal totalTaxAmount;
    private BigDecimal totalAmount;
    private int quantity;

    public Item(){
        this.taxRate = new BigDecimal("0.1");
        this.importRate = new BigDecimal("0.05");
    }

    public Item(String s){
        this.value = s;
        initializeList();
        if (this.isImport())
            this.importRate = new BigDecimal("0.05");
        else
            this.importRate = new BigDecimal("0.00");
        if (this.isExempt())
            this.taxRate = new BigDecimal("0.00");
        else
            this.taxRate = new BigDecimal("0.10");
        String [] splitArray = s.split(" ");
        this.quantity = Integer.parseInt(splitArray[0]);
        this.totalAmount = new BigDecimal(splitArray[splitArray.length - 1]);
    }

    public boolean isImport(){
        return this.value.contains("import");
    }

    public boolean isExempt(){
        for (Map.Entry<String, Integer> entry : this.exemptList.entrySet()){
            if (this.value.contains(entry.getKey()))
                return true;       
        }
        return false;
    }

    public void calculateTax(){
        BigDecimal importValue = this.importRate.multiply(this.totalAmount);
        BigDecimal tax = this.taxRate.multiply(this.totalAmount);
        this.totalTaxAmount = new BigDecimal(Math.ceil(importValue.add(tax).doubleValue()*20)/20);
        this.totalAmount = this.totalAmount.add(this.totalTaxAmount).setScale(2, RoundingMode.HALF_UP);
    }

    public void changeTaxRate(String newRate){
        this.taxRate = new BigDecimal(newRate);
    }

    public void changeImportRate(String newRate){
        this.importRate = new BigDecimal(newRate);
    }

    public void initializeList(){
        this.exemptList = new HashMap();
        this.exemptList.put("book", 1);
        this.exemptList.put("chocolate", 1);
        this.exemptList.put("pill", 1);
    }

    public void addExemption(String s){
        this.exemptList.put(s, 1);
    }

    public void removeExemption(String s){
        this.exemptList.remove(s);
    }

    public void clearExemptionList(){
        this.exemptList = null;
    }

    public BigDecimal getTotalTaxAmount(){
        return this.totalTaxAmount.setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getTotalAmount(){
        return this.totalAmount.setScale(2, RoundingMode.HALF_UP);
    }

    public void setQuantity(int n){
        this.quantity = n;
    }

    public int getQuantity(){
        return this.quantity;
    }

    public String getStringValue(){
        return this.value;
    }
}