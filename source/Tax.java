import java.util.*;
import java.io.*;

public class Tax{

	public static void main(String args []){
		Cart cart = new Cart();
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("../testing/inputFile1.txt"));
			String string = "";
			while ((string = in.readLine()) != null){
				cart.addToCart(string);
			}
			in.close();
			cart.displayCart();
		}
		catch (Exception ex)
		{
			System.out.println(ex);
		}
	} 
}