import java.util.*;
import java.math.BigDecimal;

public class Cart{
	private List<Item> items;

	public Cart(){
		this.items = new ArrayList<Item>();
	}

	public void addToCart(String s){
		Item item = new Item(s);
		item.calculateTax();
		this.items.add(item);
	}

	public List<Item> getCart(){
		return this.items;
	}
        
	public void displayCart(){
		BigDecimal taxAmount = new BigDecimal("0.00");
		BigDecimal totalAmount = new BigDecimal("0.00");
		for (Item item : this.items){
			String itemString = item.getStringValue();
			BigDecimal quantity = new BigDecimal(item.getQuantity());
			if (itemString.contains(" at "))
				System.out.println(itemString.split(" at ")[0] + ": " + item.getTotalAmount());   
			taxAmount = taxAmount.add(item.getTotalTaxAmount()).multiply(quantity);
			totalAmount = totalAmount.add(item.getTotalAmount()).multiply(quantity);
		}
		System.out.println("Sales Taxes: " + taxAmount);
		System.out.println("Total: " + totalAmount);
	}
}